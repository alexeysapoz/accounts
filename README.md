Simple application for money transfers between accounts.

### Run tests ###

To run all tests execute `sbt test jcstress:run`

### Build fat jar ###

To create jar file with all dependencies execute `sbt assembly`. Result file will be saved in `/target/scala-2.12/` directory.

### Run ###

To run application using sbt execute `sbt run`.
Jar file can be run using `java -jar ${path to jar}` call

Applicatio will listen port 8080. You can override it by redefining config parameter `http.port` in `${project path}/src/main/resources/reference.conf` or in `${classpath}/application.conf` 

### Explore ###

After running application you can see API methods description on page `http://localhost:8080/swagger`