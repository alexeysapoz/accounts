package ru.alexeysapoz.accounting.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import ru.alexeysapoz.accounting.model.{Account, AccountNumber, Money, TransferResult}
import spray.json._

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val moneyFormat: RootJsonFormat[Money] = jsonFormat2(Money)
  implicit val accountNumberFormat: RootJsonFormat[AccountNumber] = new RootJsonFormat[AccountNumber] {
    def read(json: JsValue): AccountNumber = json match {
      case JsNumber(accountNumber) =>
        new AccountNumber(accountNumber.toInt)
      case _ => deserializationError("Account number expected")
    }

    def write(accountNumber: AccountNumber): JsValue = JsNumber(accountNumber.value)
  }
  implicit val accountFormat: RootJsonFormat[Account] = jsonFormat3(Account)
  implicit val transferResultFormat: RootJsonFormat[TransferResult] = jsonFormat2(TransferResult)
  implicit val errorResponseFormat: RootJsonFormat[ErrorResponse] = jsonFormat1(ErrorResponse)
}

object JsonSupport extends JsonSupport