package ru.alexeysapoz.accounting.http

import scala.reflect.runtime.{universe => ru}
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.github.swagger.akka.{HasActorSystem, SwaggerHttpService}

class ApiDocsController(override implicit val actorSystem: ActorSystem,
                        override implicit val materializer: ActorMaterializer) extends SwaggerHttpService with HasActorSystem {
  override val apiTypes: Seq[ru.Type] = Seq(ru.typeOf[AccountsController])
  override val basePath = "/"
  override val apiDocsPath = "api-doc"
}
