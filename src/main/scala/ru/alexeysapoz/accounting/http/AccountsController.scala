package ru.alexeysapoz.accounting.http

import javax.ws.rs.Path

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, ExceptionHandler, Route}
import io.swagger.annotations._
import ru.alexeysapoz.accounting.AccountsService
import ru.alexeysapoz.accounting.model.{Account, AccountNumber, Money, TransferResult}
import ru.alexeysapoz.accounting.http.JsonSupport._
import ru.alexeysapoz.accounting.http.CustomFromStringUnmarshallers._

@Api("Operations with accounts")
@Path("/accounts")
class AccountsController(accountsService: AccountsService) extends Directives {

  @ApiOperation(httpMethod = "GET", response = classOf[Account], value = "Returns account by number")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "accountNumber", required = true, dataType = "integer", paramType = "path",
      value = "number of account to be fetched")
  ))
  @Path("/{accountNumber}")
  def getAccount: Route =
    get {
      path(IntNumber) { accountNumber =>
        accountsService.get(AccountNumber(accountNumber))
          .map(complete(_))
          .getOrElse(complete(StatusCodes.NotFound, ErrorResponse(Some("Can't find account"))))
      }
    }

  @ApiOperation(httpMethod = "POST", response = classOf[Account], value = "Returns opened account")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "amount", required = true, dataType = "BigDecimal", paramType = "query",
      value = "initial balance amount"),
    new ApiImplicitParam(name = "currency", required = true, dataType = "String", paramType = "query",
      value = "account currency")
  ))
  @Path("/open")
  def openAccount: Route =
    path("open") {
      post {
        parameter('amount.as[BigDecimal], 'currency.as[String]) { (amount, currency) =>
          complete(accountsService.open(Money(amount, currency)))
        }
      }
    }


  @ApiOperation(httpMethod = "POST", response = classOf[Account],
    value = "Add funds to account. Returns result account state")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "accountNumber", required = true, dataType = "integer", paramType = "path",
      value = "number of account"),
    new ApiImplicitParam(name = "amount", required = true, dataType = "BigDecimal", paramType = "query",
      value = "amount of money to add"),
    new ApiImplicitParam(name = "currency", required = true, dataType = "String", paramType = "query",
      value = "currency of money to add")
  ))
  @Path("/{accountNumber}/add_funds")
  def addFunds: Route =
    post {
      path(IntNumber / "add_funds") { accountNumber =>
        parameter('amount.as[BigDecimal], 'currency.as[String]) { (amount, currency) =>
          complete(accountsService.addFunds(AccountNumber(accountNumber), Money(amount, currency)))
        }
      }
    }


  @ApiOperation(httpMethod = "POST", response = classOf[TransferResult],
    value = "Returns result states of source and destination accounts created account")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "source", required = true, dataType = "integer", paramType = "query",
      value = "number of source account"),
    new ApiImplicitParam(name = "destination", required = true, dataType = "integer", paramType = "query",
      value = "number of destination account"),
    new ApiImplicitParam(name = "amount", required = true, dataType = "BigDecimal", paramType = "query",
      value = "amount of money to transfer"),
    new ApiImplicitParam(name = "currency", required = true, dataType = "String", paramType = "query",
      value = "currency of money to transfer")
  ))
  @Path("/transfer")
  def transfer: Route =
    post {
      path("transfer") {
        parameters('source.as[Int], 'destination.as[Int], 'amount.as[BigDecimal], 'currency.as[String]) {
          (src, dst, amount, currency) =>
            complete(accountsService.transfer(AccountNumber(src), AccountNumber(dst), Money(amount, currency)))
        }
      }
    }

  @ApiOperation(httpMethod = "DELETE", response = classOf[Account], value = "Closes account by number")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "accountNumber", required = true, dataType = "integer", paramType = "path",
      value = "number of account to be closed")
  ))
  @Path("/{accountNumber}")
  def close: Route =
    delete {
      path(IntNumber) { accountNumber =>
        accountsService.close(AccountNumber(accountNumber))
        complete(StatusCodes.NoContent)
      }
    }

  val accountsExceptionHandler = ExceptionHandler {
    case e: IllegalArgumentException  => complete(StatusCodes.BadRequest, ErrorResponse(Option(e.getMessage)))
    case e: NoSuchElementException => complete(StatusCodes.NotFound, ErrorResponse(Option(e.getMessage)))
    case e: IllegalStateException => complete(StatusCodes.InternalServerError, ErrorResponse(Option(e.getMessage)))
    case e: ArithmeticException => complete(StatusCodes.InternalServerError, ErrorResponse(Option(e.getMessage)))
  }

  val route: Route =
    handleExceptions(accountsExceptionHandler) {
      pathPrefix("accounts") {
        getAccount ~
          openAccount ~
          addFunds ~
          transfer ~
          close
      }
    }

}
