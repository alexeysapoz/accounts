package ru.alexeysapoz.accounting.http

import akka.http.scaladsl.unmarshalling.Unmarshaller

trait CustomFromStringUnmarshallers {

  implicit val bigDecimalFromStringUnmarshaller =
    numberUnmarshaller(string => BigDecimal(string), "BigDecimal")

  private def numberUnmarshaller[T](f: String ⇒ T, target: String): Unmarshaller[String, T] =
    Unmarshaller.strict[String, T] { string ⇒
      try f(string)
      catch numberFormatError(string, target)
    }

  private def numberFormatError(value: String, target: String): PartialFunction[Throwable, Nothing] = {
    case e: NumberFormatException ⇒
      throw
        if (value.isEmpty) {
          Unmarshaller.NoContentException
        } else {
          new IllegalArgumentException(s"'$value' is not a valid $target value", e)
        }
  }
}

object CustomFromStringUnmarshallers extends CustomFromStringUnmarshallers

