package ru.alexeysapoz.accounting

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import ru.alexeysapoz.accounting.http.ApiDocsController

object Main extends App {

  implicit val actorSystem = ActorSystem("accounting-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = actorSystem.dispatcher

  val accountingModule = AccountingModule

  val apiDocsController = new ApiDocsController

  val route =
    AccountingModule.accountsController.route ~
      apiDocsController.routes ~
      path("swagger") { getFromResource("swagger/index.html") } ~
      getFromResourceDirectory("swagger")

  Http().bindAndHandle(handler = route, interface = Config.httpInterface, port = Config.httpPort)
}
