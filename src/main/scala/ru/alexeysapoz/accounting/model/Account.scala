package ru.alexeysapoz.accounting.model

case class Account(accountNumber: AccountNumber,
                   balance: Money,
                   isClosed: Boolean)
