package ru.alexeysapoz.accounting.model

case class TransferResult(source: Account, destination: Account)
