package ru.alexeysapoz.accounting.model

case class Money(amount: BigDecimal,
                 currency: String) {

  def hasSameCurrencyAs(money: Money): Boolean = {
    money.currency == currency
  }

  def increaseAmount(amoun: BigDecimal): Money = {
    if (amoun > 0) {
      copy(amount = this.amount + amoun)
    } else {
      throw new IllegalArgumentException("Positive number should be passed to increase money amount")
    }
  }

  def decreaseAmount(amount: BigDecimal): Money = {
    if (amount > 0) {
      copy(amount = this.amount - amount)
    } else {
      throw new IllegalArgumentException("Positive number should be passed to decrease money amount")
    }
  }
}

