package ru.alexeysapoz.accounting

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.ReentrantReadWriteLock

import ru.alexeysapoz.accounting.model.{Account, AccountNumber, Money, TransferResult}

class AccountsService {

  private class AccountRecord(val number: AccountNumber,
                              private var account: Account) {
    private[this] val readWriteLock = new ReentrantReadWriteLock()
    private[this] val readLock = readWriteLock.readLock()
    private val writeLock = readWriteLock.writeLock()

    def getAccount = {
      readLock.lock()
      try { account }
      finally { readLock.unlock() }
    }

    def update(updater: Account => Account): Account = {
      writeLock.lock()
      try {
        account = updater(account)
        account
      }
      finally { writeLock.unlock() }
    }

    def updatePair(anotherRecord: AccountRecord)
                  (updater: (Account, Account) => (Account, Account)): (Account, Account) = {
      writeLockPair(this, anotherRecord)
      try {
        val (updatedThisRecordsAccount, updatedAnotherRecordsAccount) = updater(account, anotherRecord.account)
        account = updatedThisRecordsAccount
        anotherRecord.account = updatedAnotherRecordsAccount

        (updatedThisRecordsAccount, updatedAnotherRecordsAccount)
      }
      finally {
        writeUnlockPair(anotherRecord, this)
      }
    }

    private[this] def writeLockPair(record1: AccountRecord, record2: AccountRecord): Unit = {
      if(record1.number.value > record2.number.value) {
        record1.writeLock.lock()
        record2.writeLock.lock()
      } else {
        record2.writeLock.lock()
        record1.writeLock.lock()
      }
    }

    private[this] def writeUnlockPair(record1: AccountRecord, record2: AccountRecord): Unit = {
      if(record1.number.value > record2.number.value) {
        record2.writeLock.unlock()
        record1.writeLock.unlock()
      } else {
        record1.writeLock.unlock()
        record2.writeLock.unlock()
      }
    }
  }

  private[this] val accountNumberSequence = new AtomicInteger(0)
  private[this] val accountsRecords = new java.util.concurrent.ConcurrentHashMap[AccountNumber, Option[AccountRecord]]()

  private def getRecord(accountNumber: AccountNumber) = accountsRecords.getOrDefault(accountNumber, None)

  def open(initialBalance: Money): Account = {
    val newAccountNumber = AccountNumber(accountNumberSequence.getAndIncrement())
    val account = Account(newAccountNumber, initialBalance, isClosed = false)
    val record = new AccountRecord(newAccountNumber, account)
    accountsRecords.put(newAccountNumber, Some(record))
    account
  }

  def get(accountNumber: AccountNumber): Option[Account] = {
    getRecord(accountNumber).map(_.getAccount)
  }

  def addFunds(accountNumber: AccountNumber, moneyToAdd: Money): Account = {
    getRecord(accountNumber)
      .getOrElse(throw new NoSuchElementException(s"Account ${accountNumber.value} not found"))
      .update { account =>
        if (account.isClosed) {
          throw new IllegalStateException(s"Account ${accountNumber.value} is closed")
        }

        if (account.balance.currency == moneyToAdd.currency) {
          account.copy(
            balance = account.balance.increaseAmount(moneyToAdd.amount)
          )
        } else {
          throw new IllegalArgumentException("Passed currency and account currency are different")
        }
      }
  }

  def transfer(source: AccountNumber, destination: AccountNumber, moneyToTransfer: Money): TransferResult = {
    if(source == destination) {
      throw new IllegalArgumentException("Source and destination account numbers should be different")
    }

    val sourceAccountRecord = getRecord(source)
      .getOrElse(throw new NoSuchElementException(s"Can't find source account with number ${source.value}"))

    val destinationAccountRecord = getRecord(destination)
      .getOrElse(throw new NoSuchElementException(s"Can't find destination account with number ${destination.value}"))

    val (resultSourceAccount, resultDestinationAccount) =
      sourceAccountRecord.updatePair(destinationAccountRecord) { case (sourceAccount, destinationAccount) =>
        validateTransferData(sourceAccount, destinationAccount, moneyToTransfer)

        val resultSourceAccountBalance =
          sourceAccount.balance.decreaseAmount(moneyToTransfer.amount)
        val resultDestinationAccountBalance =
          destinationAccount.balance.increaseAmount(moneyToTransfer.amount)

        val resultSourceAccount = sourceAccount.copy(balance = resultSourceAccountBalance)
        val resultDestinationAccount = destinationAccount.copy(balance = resultDestinationAccountBalance)

        (resultSourceAccount, resultDestinationAccount)
      }

    TransferResult(
      source = resultSourceAccount,
      destination = resultDestinationAccount
    )
  }

  private def validateTransferData(sourceAccount: Account,
                                   destinationAccount: Account,
                                   moneyToTransfer: Money): Unit = {
    if (!sourceAccount.balance.hasSameCurrencyAs(destinationAccount.balance)) {
      throw new IllegalArgumentException("Different currencies of source and destination accounts")
    }

    if (!sourceAccount.balance.hasSameCurrencyAs(moneyToTransfer)) {
      throw new IllegalArgumentException("Currency of transferred funds differs from accounts currency")
    }

    if (moneyToTransfer.amount < 0) {
      throw new IllegalArgumentException("Amount to transfer should be positive")
    }

    if (sourceAccount.balance.amount < moneyToTransfer.amount) {
      throw new IllegalStateException("Insufficient funds in source account")
    }

    if (sourceAccount.isClosed) {
      throw new IllegalStateException(s"Source account ${sourceAccount.accountNumber.value} is already closed")
    }

    if (destinationAccount.isClosed) {
      throw new IllegalStateException(s"Destination account ${destinationAccount.accountNumber.value} is already closed")
    }
  }

  def close(accountNumber: AccountNumber): Unit = {
    getRecord(accountNumber)
      .getOrElse(throw new NoSuchElementException(s"Account ${accountNumber.value} not found"))
      .update { account =>
        if(!account.isClosed) {
          account.copy(isClosed = true)
        } else {
          throw new IllegalStateException(s"Account ${accountNumber.value} is already closed")
        }
      }
  }
}
