package ru.alexeysapoz.accounting

import ru.alexeysapoz.accounting.http.AccountsController

object AccountingModule {
  val accountsService: AccountsService = new AccountsService()

  val accountsController: AccountsController = new AccountsController(accountsService)
}
