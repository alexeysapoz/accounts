package ru.alexeysapoz.accounting

import com.typesafe.config.ConfigFactory

object Config {
  private val config = ConfigFactory.load()

  def httpPort: Int = config.getInt("http.port")
  def httpInterface: String = config.getString("http.interface")
}
