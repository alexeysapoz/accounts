package ru.alexeysapoz.accounting

import java.util.concurrent.TimeoutException

import org.openjdk.jcstress.annotations.Outcome.Outcomes
import org.openjdk.jcstress.annotations._
import org.openjdk.jcstress.infra.results._
import ru.alexeysapoz.accounting.model.{AccountNumber, Money}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object AccountsServiceAtomicityTest {

  @JCStressTest
  @Description("Check account number atomicity")
  @Outcomes(Array(
    new Outcome(id = Array("0, 1"), expect = Expect.ACCEPTABLE, desc = "Accounts have different numbers"),
    new Outcome(id = Array("1, 0"), expect = Expect.ACCEPTABLE, desc = "Accounts have different numbers"),
    new Outcome(id = Array("0, 0"), expect = Expect.FORBIDDEN, desc = "accounts have same numbers")
  ))
  @State
  class AccountOpening {
    val accountsService = new AccountsService()

    @Actor
    def openAccount1(r: II_Result): Unit = {
      r.r1 = accountsService.open(Money(100, "usd")).accountNumber.value

    }

    @Actor
    def openAccount2(r: II_Result): Unit = {
      r.r2 = accountsService.open(Money(10, "eur")).accountNumber.value
    }
  }

  @JCStressTest
  @Description("There should not be opened but empty account")
  @Outcomes(Array(
    new Outcome(id = Array("None"), expect = Expect.ACCEPTABLE, desc = "Account doesn't exist"),
    new Outcome(id = Array("Some(Account(AccountNumber(0),Money(100,usd),false))"), expect = Expect.ACCEPTABLE, desc = "account exists and it's empty")
  ))
  @State
  class OpenedAccountVisibility {
    val accountsService = new AccountsService()

    @Actor
    def openAccount(): Unit = {
      accountsService.open(Money(100, "usd"))
    }

    @Actor
    def getOpenedAccountState(r: L_Result): Unit = {
      r.r1 = accountsService.get(AccountNumber(0))
    }

  }


  @JCStressTest
  @Description("Transfer from account should be atomic")
  @Outcomes(Array(
    new Outcome(
      id = Array("first transfer: result balance 99, second transfer: failed"), expect = Expect.ACCEPTABLE,
      desc = "Successful first transfer and failed second transfer (insufficient funds)"),
    new Outcome(
      id = Array("first transfer: failed, second transfer: result balance 98"), expect = Expect.ACCEPTABLE,
      desc = "Successful second transfer and failed first transfer (insufficient funds)"),
    new Outcome(
      id = Array("first transfer: result balance -803, second transfer: result balance 98"), expect = Expect.FORBIDDEN,
      desc = "Both transfers are successful. Result balance of source account is negative"),
    new Outcome(
      id = Array("first transfer: result balance 99, second transfer: result balance -803"), expect = Expect.FORBIDDEN,
      desc = "Both transfers are successful. Result balance of source account is negative"),
    new Outcome(
      id = Array("first transfer: result balance 99, second transfer: result balance 98"), expect = Expect.FORBIDDEN,
      desc = "Both transfers are successful. Lost charge from source account")
  ))
  @State
  class SimultaneousTransfersFromSameAccount {

    val service = new AccountsService()
    val sourceAccount = service.open(Money(1000, "usd"))
    val destAccount = service.open(Money(200, "usd"))

    @Actor
    def transfer1(r: LL_Result): Unit = {
      try {
        val transferResult = service.transfer(sourceAccount.accountNumber, destAccount.accountNumber, Money(901, "usd"))
        r.r1 = s"first transfer: result balance ${transferResult.source.balance.amount}"
      } catch {
        case _: IllegalStateException => r.r1 = s"first transfer: failed"
      }
    }

    @Actor
    def transfer2(r: LL_Result): Unit = {
      try {
        val transferResult = service.transfer(sourceAccount.accountNumber, destAccount.accountNumber, Money(902, "usd"))
        r.r2 = s"second transfer: result balance ${transferResult.source.balance.amount}"
      } catch {
        case _: IllegalStateException => r.r2 = s"second transfer: failed"
      }
    }
  }

  @JCStressTest
  @Description("There shouldn't be deadlocks o cyclic transfers")
  @Outcomes(Array(
    new Outcome(
      id = Array("transfer account1 -> account2 is successful, transfer account2 -> account1 is successful"),
      expect = Expect.ACCEPTABLE,
      desc = "Both transfers are successful"),
    new Outcome(
      id = Array("transfer account1 -> account2 takes too much time, transfer account2 -> account1 takes too much time"),
      expect = Expect.FORBIDDEN,
      desc = "Deadlock happened")
  ))
  @State
  class CyclicTransfers {

    val service = new AccountsService()
    val account1 = service.open(Money(10, "usd"))
    val account2 = service.open(Money(20, "usd"))

    @Actor
    def transferTo(r: LL_Result): Unit = {
      withTimeout(
        operation = {
          service.transfer(account1.accountNumber, account2.accountNumber, Money(1, "usd"))
          r.r1 = "transfer account1 -> account2 is successful"
        },
        onTimeout = {
        r.r1 = "transfer account1 -> account2 takes too much time"
      })
    }

    @Actor
    def transferFrom(r: LL_Result): Unit = {
      withTimeout(
        operation = {
          service.transfer(account2.accountNumber, account1.accountNumber, Money(2, "usd"))
          r.r2 = "transfer account2 -> account1 is successful"
        },
        onTimeout = {
          r.r2 = "transfer account2 -> account1 takes too much time"
        })
    }


    private def withTimeout(operation: => Unit, onTimeout: => Unit): Unit = {
      try {
        Await.result(Future(operation), 100.millis)
      } catch {
        case _: TimeoutException => onTimeout
      }
    }
  }

}
