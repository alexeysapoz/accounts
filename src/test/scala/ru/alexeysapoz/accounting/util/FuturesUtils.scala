package ru.alexeysapoz.accounting.util

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

trait FuturesUtils {

  def await[T](future: Future[T]): T = Await.result(future, Duration.Inf)

}
