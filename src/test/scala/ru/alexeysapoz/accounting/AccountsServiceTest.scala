package ru.alexeysapoz.accounting

import org.scalatest.Inspectors._
import org.scalatest.Matchers._
import org.scalatest.FlatSpec
import ru.alexeysapoz.accounting.model.{Account, AccountNumber, TransferResult}
import ru.alexeysapoz.accounting.util.FuturesUtils

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class AccountsServiceTest extends FlatSpec with FuturesUtils {

  behavior of "AccountsService"

  "open" should "open account with provided balance and return it's state" in new AccountsServiceTestData {
    val initialBalance = eur(1000)

    val resultAccount = service.open(initialBalance)
    resultAccount.balance shouldBe initialBalance

    service.get(resultAccount.accountNumber) shouldBe Some(resultAccount)
  }

  it should "open accounts with sequential account numbers" in new AccountsServiceTestData {
    val numberOfAccountsToCreate = 50

    forAll(0 to numberOfAccountsToCreate) { expectedNumber =>
      val expectedAccountNumber = AccountNumber(expectedNumber)

      service.get(expectedAccountNumber) shouldBe None
      val newAccount = service.open(eur(10))
      newAccount.accountNumber shouldBe expectedAccountNumber
      service.get(expectedAccountNumber).get shouldBe newAccount
    }
  }

  it should "not generate duplicate account numbers on concurrent calls" in new AccountsServiceTestData {
    val numberOfAccountsToCreate = 10000

    val createdAccounts: Seq[Account] = await {
      Future.traverse(0 to numberOfAccountsToCreate) { _ =>
        Future(service.open(eur(10)))
      }
    }

    val accountNumbers = createdAccounts.map(_.accountNumber.value)

    accountNumbers.distinct shouldBe accountNumbers
    createdAccounts.map(_.accountNumber.value) should contain theSameElementsAs(0 to numberOfAccountsToCreate)
  }

  "get" should "return None if requested account doesn't exist" in new AccountsServiceTestData {
    serviceWithFewAccounts.get(nonExistingAccountNumber) shouldBe None
  }

  "addFunds" should "add money to existing account and return result account state" in new AccountsServiceTestData {
    val initialBalance = serviceWithFewAccounts.get(usdAccount1Number).get.balance

    val returnedBalance = serviceWithFewAccounts.addFunds(usdAccount1Number, usd(123)).balance
    val resultBalance = serviceWithFewAccounts.get(usdAccount1Number).get.balance

    returnedBalance shouldBe resultBalance
    resultBalance shouldBe usd(1234.11)
  }

  it should "fail if account with specified number doesn't exist or account is closed" in new AccountsServiceTestData {
    intercept[NoSuchElementException] {
      serviceWithFewAccounts.addFunds(nonExistingAccountNumber, usd(123))
    }.getMessage shouldBe "Account 9999 not found"

    intercept[IllegalStateException] {
      serviceWithFewAccounts.addFunds(closedEurAccountNumber, eur(123))
    }.getMessage shouldBe "Account 3 is closed"

    verifyThatAccountsStatesHasNotChanged()
  }

  it should "fail if currencies of account and added money are different" in new AccountsServiceTestData {
    intercept[IllegalArgumentException] {
      serviceWithFewAccounts.addFunds(eurAccountNumber, usd(123))
    }.getMessage shouldBe "Passed currency and account currency are different"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "check passed amount" in new AccountsServiceTestData {
    intercept[IllegalArgumentException] {
      serviceWithFewAccounts.addFunds(usdAccount1Number, usd(-123))
    }.getMessage shouldBe "Positive number should be passed to increase money amount"
    verifyThatAccountsStatesHasNotChanged()
  }

  "transfer" should "not allow transfers to the same account" in new AccountsServiceTestData {
    intercept[IllegalArgumentException] {
      serviceWithFewAccounts.transfer(usdAccount1Number, usdAccount1Number, usd(10))
    }.getMessage shouldBe "Source and destination account numbers should be different"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "not allow transfers from non existing accounts" in new AccountsServiceTestData {
    intercept[NoSuchElementException] {
      serviceWithFewAccounts.transfer(nonExistingAccountNumber, usdAccount2Number, usd(10))
    }.getMessage shouldBe "Can't find source account with number 9999"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "not allow transfers from closed accounts" in new AccountsServiceTestData {
    intercept[IllegalStateException] {
      serviceWithFewAccounts.transfer(closedEurAccountNumber, eurAccountNumber, eur(10))
    }.getMessage shouldBe s"Source account 3 is already closed"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "not allow transfers to non existing accounts" in new AccountsServiceTestData {
    intercept[NoSuchElementException] {
      serviceWithFewAccounts.transfer(eurAccountNumber, nonExistingAccountNumber, eur(10))
    }.getMessage shouldBe "Can't find destination account with number 9999"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "not allow transfers to closed accounts" in new AccountsServiceTestData {
    intercept[IllegalStateException] {
      serviceWithFewAccounts.transfer(eurAccountNumber, closedEurAccountNumber, eur(10))
    }.getMessage shouldBe s"Destination account 3 is already closed"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "not allow transfers with negative amounts" in new AccountsServiceTestData {
    intercept[IllegalArgumentException] {
      serviceWithFewAccounts.transfer(usdAccount1Number, usdAccount2Number, usd(-10))
    }.getMessage shouldBe "Amount to transfer should be positive"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "check that transferred amount isn't more than balance of source account" in new AccountsServiceTestData {
    intercept[IllegalStateException] {
      serviceWithFewAccounts.transfer(usdAccount1Number, usdAccount2Number, usd(1211.11))
    }.getMessage shouldBe "Insufficient funds in source account"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "not allow transfers between accounts with different currencies" in new AccountsServiceTestData {
    intercept[IllegalArgumentException] {
      serviceWithFewAccounts.transfer(usdAccount1Number, eurAccountNumber, usd(12.11))
    }.getMessage shouldBe "Different currencies of source and destination accounts"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "check currency of transferred money" in new AccountsServiceTestData {
    intercept[IllegalArgumentException] {
      serviceWithFewAccounts.transfer(usdAccount1Number, usdAccount2Number, eur(12.11))
    }.getMessage shouldBe "Currency of transferred funds differs from accounts currency"
    verifyThatAccountsStatesHasNotChanged()
  }

  it should "make transfer and return result state of both accounts" in new AccountsServiceTestData {
    val expectedResult = TransferResult(
      source = usdAccount1.copy(balance = usd(1098.66)),
      destination = usdAccount2.copy(balance = usd(2234.67))
    )

    val result = serviceWithFewAccounts.transfer(usdAccount1Number, usdAccount2Number, usd(12.45))

    result shouldBe expectedResult

    serviceWithFewAccounts.get(usdAccount1Number).get shouldBe expectedResult.source
    serviceWithFewAccounts.get(usdAccount2Number).get shouldBe expectedResult.destination
  }

  "close" should "make account closed" in new AccountsServiceTestData {
    serviceWithFewAccounts.close(usdAccount1Number)
    serviceWithFewAccounts.get(usdAccount1Number).get.isClosed shouldBe true
  }

  it should "return error if account doesn't exist" in new AccountsServiceTestData {
    intercept[NoSuchElementException] {
      serviceWithFewAccounts.close(nonExistingAccountNumber)
    }.getMessage shouldBe "Account 9999 not found"
  }

  it should "return error if account is already closed" in new AccountsServiceTestData {
    intercept[IllegalStateException] {
      serviceWithFewAccounts.close(closedEurAccountNumber)
    }.getMessage shouldBe "Account 3 is already closed"
  }

}
