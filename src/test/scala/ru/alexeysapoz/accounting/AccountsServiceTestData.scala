package ru.alexeysapoz.accounting

import org.scalatest.Matchers._
import ru.alexeysapoz.accounting.model.{Account, AccountNumber, Money}

trait AccountsServiceTestData {

  def usd(amount: BigDecimal): Money = Money(amount, "USD")
  def eur(amount: BigDecimal): Money = Money(amount, "EUR")

  val service = new AccountsService()

  val serviceWithFewAccounts = new AccountsService()

  val usdAccount1: Account = serviceWithFewAccounts.open(usd(1111.11))
  val usdAccount1Number: AccountNumber = usdAccount1.accountNumber

  val usdAccount2: Account = serviceWithFewAccounts.open(usd(2222.22))
  val usdAccount2Number: AccountNumber = usdAccount2.accountNumber

  val eurAccount: Account = serviceWithFewAccounts.open(eur(3333.33))
  val eurAccountNumber: AccountNumber = eurAccount.accountNumber

  val closedEurAccountNumber: AccountNumber = serviceWithFewAccounts.open(eur(10)).accountNumber
  serviceWithFewAccounts.close(closedEurAccountNumber)

  val nonExistingAccountNumber:AccountNumber = AccountNumber(9999)

  def verifyThatAccountsStatesHasNotChanged(): Unit = {
    serviceWithFewAccounts.get(usdAccount1Number).get shouldBe usdAccount1
    serviceWithFewAccounts.get(usdAccount2Number).get shouldBe usdAccount2
    serviceWithFewAccounts.get(eurAccountNumber).get shouldBe eurAccount
  }
}
