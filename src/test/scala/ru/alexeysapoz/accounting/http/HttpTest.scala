package ru.alexeysapoz.accounting.http

import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.Suite
import org.scalatest.matchers.{MatchResult, Matcher}
import spray.json._

import scala.util.Try

trait HttpTest extends ScalatestRouteTest with CustomMatchers { this: Suite =>

}

trait CustomMatchers {
  class JsonAreEquals(expectedJson: String) extends Matcher[String] {

    override def apply(left: String): MatchResult = {
      val expectedJsonTry = Try(expectedJson.parseJson)
      val expectedJsonErrorMessage =
        expectedJsonTry
          .map { expected =>
            s"""|Expected JSON:
                |${expected.prettyPrint}""".stripMargin
          }.getOrElse(
            s"""|Can't parse string with expected json. Passed value:
                |"$expectedJson"""".stripMargin)

      val testedJsonTry = Try(left.parseJson)
      val testedJsonErrorMessage =
        testedJsonTry
          .map { tested =>
            s"""|Actual JSON:
                |${tested.prettyPrint}""".stripMargin
          }
          .getOrElse(
            s"""|Can't parse string with tested json. Passed value:
                |"$left"
           """.stripMargin)


      MatchResult(
        expectedJsonTry == testedJsonTry,
        rawFailureMessage =
          s"""Tested Json does not match to expected JSON.
             |
             |$expectedJsonErrorMessage
             |
             |$testedJsonErrorMessage
             |
           """.stripMargin,
        rawNegatedFailureMessage = "Expected and tested JSONs are equal"
      )
    }


  }

  def beTheSameJsonAs(expectedJson: String): Matcher[String] = new JsonAreEquals(expectedJson)
}