package ru.alexeysapoz.accounting.http

import akka.http.scaladsl.model.StatusCodes
import org.scalamock.scalatest.MockFactory
import org.scalatest.Matchers._
import org.scalatest.FlatSpec
import ru.alexeysapoz.accounting.{AccountsService, AccountsServiceTestData}


class AccountsControllerTest extends FlatSpec with HttpTest with MockFactory {

  trait TestData extends AccountsServiceTestData {

    val controller = new AccountsController(serviceWithFewAccounts)

    val accountsServiceMock = mock[AccountsService]
    val controllerWithMock = new AccountsController(accountsServiceMock)

    val route = controller.route
  }

  behavior of "AccountsController"

  it should "open account and retsbturn initial account state" in new TestData {
    Post("/accounts/open?amount=10&currency=RUB") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[String] should beTheSameJsonAs (
        """{
             "accountNumber": 4,
             "balance": {
               "amount": 10,
               "currency": "RUB"
             },
             "isClosed": false
           }"""
      )
    }
  }

  it should "return account state" in new TestData {
    Get("/accounts/2") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[String] should beTheSameJsonAs (
        """{
             "accountNumber": 2,
             "balance": {
               "amount": 3333.33,
               "currency": "EUR"
             },
             "isClosed": false
           }"""
      )
    }
  }

  it should "return proper status if account wasn't found" in new TestData {
    Get("/accounts/9999") ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[String] should beTheSameJsonAs (
        """{
             "errorMessage": "Can't find account"
           }"""
      )
    }
  }

  it should "add funds to account and return result account state" in new TestData {
    Post("/accounts/2/add_funds?amount=13.34&currency=EUR") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[String] should beTheSameJsonAs (
        """{
             "accountNumber": 2,
             "balance": {
               "amount": 3346.67,
               "currency": "EUR"
             },
             "isClosed": false
           }"""
      )
    }
  }

  it should "transfer funds from one account to another and return result accounts state" in new TestData {
    Post("/accounts/transfer?source=0&destination=1&amount=20.11&currency=USD") ~> route ~> check {
      status shouldBe StatusCodes.OK

      responseAs[String] should beTheSameJsonAs (
        """{
             "source": {
               "accountNumber": 0,
               "balance": {
                 "amount": 1091.00,
                 "currency": "USD"
               },
               "isClosed": false
             },
             "destination": {
               "accountNumber": 1,
               "balance": {
                 "amount": 2242.33,
                 "currency": "USD"
               },
               "isClosed": false
             }
           }"""
      )
    }
  }

  it should "close account" in new TestData {
    Delete("/accounts/2") ~> route ~> check {
      status shouldBe StatusCodes.NoContent
    }
  }

  it should "render errors" in new TestData {
    (accountsServiceMock.get _).expects(*).throwing(new IllegalArgumentException("Wrong parameter passed"))
    Get("/accounts/2") ~> controllerWithMock.route ~> check {
      status shouldBe StatusCodes.BadRequest
      responseAs[String] should beTheSameJsonAs (
        """{
             "errorMessage": "Wrong parameter passed"
           }"""
      )
    }

    (accountsServiceMock.get _).expects(*).throwing(new NoSuchElementException("Can't find account"))
    Get("/accounts/2") ~> controllerWithMock.route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[String] should beTheSameJsonAs (
        """{
             "errorMessage": "Can't find account"
           }"""
      )
    }

    (accountsServiceMock.get _).expects(*).throwing(new IllegalStateException("Account is in wrong state"))
    Get("/accounts/2") ~> controllerWithMock.route ~> check {
      status shouldBe StatusCodes.InternalServerError
      responseAs[String] should beTheSameJsonAs (
        """{
             "errorMessage": "Account is in wrong state"
           }"""
      )
    }

    (accountsServiceMock.get _).expects(*).throwing(new ArithmeticException("Some ArithmeticException"))
    Get("/accounts/2") ~> controllerWithMock.route ~> check {
      status shouldBe StatusCodes.InternalServerError
      responseAs[String] should beTheSameJsonAs (
        """{
             "errorMessage": "Some ArithmeticException"
           }"""
      )
    }
  }

}
