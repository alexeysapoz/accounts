name := "accounts"

version := "1.0"

scalaVersion := "2.12.1"

val akkaHttpVersion = "10.0.9"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe" % "config" % "1.3.1",
  "com.github.swagger-akka-http" %% "swagger-akka-http" % "0.9.2",
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",
  "org.scalatest" %% "scalatest" % "3.0.3" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.5" % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test
)

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings")

enablePlugins(JCStressPlugin)

mainClass := Some("ru.alexeysapoz.accounting.Main")